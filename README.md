# TTA GitPod VS Code


## Getting started

1. gitlab 가입
2. [TTA-CHAMP / Gitpod · GitLab](https://gitlab.com/tta-champ/gitpod) fork 주소
image.png
![fork.png](./fork.png)

3. gitlab 계정으로 gitpod.io 가입

![gitpod.png](./gitpod.png)

## docker build
- `docker build .`

## docker push
- `docker tag [tag_id] [dockerhub_id]/[name]:[version]
- `docker image ls`
- `docker login`
  - id, passwd
- `docker push [dockerhub_id]/[name]:[version]`
- `docker rmi [dockerhub_id]/[name]:[version]`


## gitlab container registry
- ` docker login registry.gitlab.com`
- `docker build -t registry.gitlab.com/tta-champ/gitpod .`
- `docker push registry.gitlab.com/tta-champ/gitpod`
